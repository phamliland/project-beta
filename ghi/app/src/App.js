import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import './Footer.css';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentList from './ServiceAppointment';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';
import ModelForm from './ModelForm';
import CustomerForm from './CustomerForm';
import DarkMode from './DarkMode';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <DarkMode />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="salespeople/list" element={<SalespersonList />} />
            <Route path="customers/list" element={<CustomerList />} />
            <Route path="sales/list" element={<SalesList />} />
            <Route path="automobiles/list" element={<AutomobileList />} />
            <Route path="technicians/list" element={<TechnicianList />} />
            <Route path="appointments/list" element={<ServiceAppointmentList />} />
            <Route path="manufacturers/list" element={<ManufacturerList />} />
            <Route path="models/list" element={<ModelsList />} />
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm />} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians">
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="create" element={<ServiceAppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="create" element={<ModelForm />} />
          </Route>
        </Routes>
        <footer className="footer">
          <div className="content-container">
            <p className="footer-text">Brought to you by KittyCar</p>
          </div>
        </footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
